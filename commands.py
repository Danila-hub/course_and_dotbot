from telegram.parsemode import ParseMode

import keyboards
from texts import messages
from parser import parse_content


def show_convert_help(update, context):
    update.message.reply_text(
        text=messages.CONVERT_HELP,
        reply_markup=keyboards.TO_START_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_currencies_help(update, context):
    update.message.reply_text(
        messages.CURRENCIES_HELP,
        reply_markup=keyboards.CURRENCIES_INLINE_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_all_currencies(update, context):
    content = parse_content()
    if content:
        text = ""
        for valute_char, valute_info in content.items():
            valute_name = valute_info.get("Name")
            valute_value = valute_info.get("Value")

            if valute_name and valute_value:
                text += (
                    f"💹 {valute_name}\n"
                    + f"Код валюты: `{valute_char}`"
                    + f"   Цена: `{valute_value}`₽\n\n"
                )
            else:
                continue
        if update.callback_query:
            update.callback_query.edit_message_text(
                text=text, parse_mode=ParseMode.MARKDOWN
            )
        else:
            update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN)
    else:
        show_parser_connection_error(update, context)


def show_current_code_error(update, context):
    update.message.reply_text(
        text=messages.CURRENT_CODE_ERROR,
        reply_markup=keyboards.CURRENCY_CODE_ERROR_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_parser_connection_error(update, context):
    update.message.reply_text(
        messages.PARSER_CONNECTION_ERROR,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_amount_success(update, context):
    update.message.reply_text(
        messages.VALID_AMOUNT,
        reply_markup=keyboards.CURRENCIES_INLINE_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_rate_success(update, content):
    update.message.reply_text(
        messages.VALID_RATE,
        reply_markup=keyboards.CURRENCIES_INLINE_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def incorrect_amount(update, context):
    update.message.reply_text(
        messages.NOT_VALID_AMOUNT,
        reply_markup=keyboards.TO_START_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def incorrect_valute_code(update, context):
    update.message.reply_text(
        messages.CURRENT_CODE_ERROR,
        reply_markup=keyboards.TO_START_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def unlogic_valute_code(update, context):
    update.message.reply_text(
        messages.UNLOGIC_CURRENT_CODE,
        reply_markup=keyboards.CURRENCIES_INLINE_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )


def show_start(update, context):
    update.message.reply_text(
        text=messages.START,
        reply_markup=keyboards.START_CHOOSE_MARKUP,
        parse_mode=ParseMode.MARKDOWN,
    )
