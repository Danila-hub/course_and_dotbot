import xml.etree.ElementTree as ET
import logging

import xmltodict
import requests
from requests.adapters import HTTPAdapter, Retry


URL = "https://www.cbr.ru/scripts/XML_daily.asp"

FILE_DATA = {"file_date": "", "content": ""}


def parse_content():
    session = requests.Session()
    retries = Retry(total=2, backoff_factor=2, status_forcelist=[500, 502, 503, 504])
    session.mount("https://", HTTPAdapter(max_retries=retries))

    try:
        response = session.get(URL)
        response.raise_for_status()
    except Exception as e:
        logging.error(e)
    else:
        response_file_date = ET.fromstring(response.text).attrib.get("Date")

        if response_file_date:
            if FILE_DATA["file_date"] == response_file_date:
                content = FILE_DATA["content"]
            else:
                current_data = xmltodict.parse(response.text)
                FILE_DATA["file_date"] = response_file_date
                FILE_DATA["content"] = {
                    item["CharCode"]: {
                        "Value": item["Value"].replace(",", "."),
                        "Name": item["Name"],
                    }
                    for item in current_data["ValCurs"]["Valute"]
                    if item.get("CharCode") and item.get("Value") and item.get("Name")
                }

                content = FILE_DATA["content"]

                logging.info(
                    f'Изменена дата файла ЦБ РФ. Актуальная дата: {FILE_DATA["file_date"]}'
                )
            return content
        else:
            logging.error('Не удалось найти атрибут "Date" в файле ЦБ РФ.')
