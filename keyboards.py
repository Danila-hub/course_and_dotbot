from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup,
)

from texts import actions


START_CHOOSE_MARKUP = ReplyKeyboardMarkup(
    [[actions.SHOW_CURRENCY, actions.SHOW_CONVERTATION], [actions.SHOW_ALL_CURRENCIES]],
    resize_keyboard=True,
)

CURRENCIES_INLINE_MARKUP = InlineKeyboardMarkup(
    [
        [
            InlineKeyboardButton(
                actions.SHOW_ALL_CURRENCIES,
                callback_data="show_all_currencies",
            )
        ],
    ]
)

CONVERTATION_CHOOSE_MARKUP = ReplyKeyboardMarkup(
    [[actions.TO_CONVERT], [actions.TO_BACK]],
    resize_keyboard=True,
)

CURRENCY_CHOOSE_MARKUP = ReplyKeyboardMarkup(
    [[actions.TO_SHOW_CURRENCY], [actions.TO_BACK]],
    resize_keyboard=True,
)

CURRENCY_CODE_ERROR_MARKUP = ReplyKeyboardMarkup(
    [[actions.SHOW_ALL_CURRENCIES], [actions.TO_BACK]],
    resize_keyboard=True,
)

TO_START_MARKUP = ReplyKeyboardMarkup(
    [[actions.TO_BACK]],
    resize_keyboard=True,
)
