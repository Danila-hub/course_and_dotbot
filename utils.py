from decimal import Decimal

from parser import parse_content


def validate_amount(text):
    if not text or len(text) > 12:
        return False

    if text[0] == "-":
        text = text[1:]

    if "." in text:
        parts = text.split(".")

        if len(parts) != 2:
            return False

        return parts[0].isdigit() and parts[1].isdigit()

    return text.isdigit()


def validate_valute_code(valute_code):
    valute_code = valute_code.upper()

    if valute_code == "RUB":
        return valute_code

    valute = parse_content().get(valute_code)

    if valute and valute.get("Name") and valute.get("Value"):
        return valute


def convert(update, user_data):
    amount = Decimal(user_data.get("amount"))

    to_rate = user_data.get("to_rate")
    from_rate = user_data.get("from_rate")

    from_rate_code = user_data.get("from_rate_code")
    to_rate_code = user_data.get("to_rate_code")

    if from_rate_code == "RUB":
        to_rate_value = Decimal(to_rate.get("Value"))
        result = (amount / to_rate_value).quantize(Decimal("1.0000"))

    elif to_rate_code == "RUB":
        from_rate_value = Decimal(from_rate.get("Value"))
        result = (amount * from_rate_value).quantize(Decimal("1.0000"))

    else:
        from_rate_value = Decimal(from_rate.get("Value"))
        to_rate_value = Decimal(to_rate.get("Value"))
        result = (amount * (from_rate_value / to_rate_value)).quantize(
            Decimal("1.0000")
        )

    return result
