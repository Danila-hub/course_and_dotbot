from telegram.parsemode import ParseMode
from telegram.ext import ConversationHandler

import commands
import keyboards
import utils
from texts import actions


(
    SHOW_CURRENCY,
    SHOW_CONVERTATION,
    AMOUNT_STATE,
    FROM_RATE_STATE,
    TO_RATE_STATE,
    CODE_CURRENCY_STATE,
) = range(6)


def start_show_currency_entry(update, context):
    commands.show_currencies_help(update, context)
    return CODE_CURRENCY_STATE


def start_convert_entry(update, context):
    commands.show_convert_help(update, context)
    return AMOUNT_STATE


def check_currency_code(update, context):
    if update.message.text == actions.SHOW_ALL_CURRENCIES:
        commands.show_all_currencies(update, context)
        return CODE_CURRENCY_STATE

    if update.message.text == actions.TO_BACK:
        commands.show_start(update, context)
        return ConversationHandler.END

    user_text = update.message.text.upper()
    valute = utils.validate_valute_code(user_text)

    if valute and valute != "RUB":
        valute_name = valute.get("Name")
        valute_value = valute.get("Value")

        context.user_data["show_currency_text"] = (
            f"\[`{user_text}`]  |  `{valute_value}`₽  |  {valute_name}"
        )

        update.message.reply_text(
            f"Выбранная валюта: {valute_name}",
            reply_markup=keyboards.CURRENCY_CHOOSE_MARKUP,
        )
        return SHOW_CURRENCY
    else:
        commands.show_current_code_error(update, context)
        return CODE_CURRENCY_STATE


def show_currency(update, context):
    user_text = update.message.text

    if user_text == actions.TO_SHOW_CURRENCY:
        update.message.reply_text(
            context.user_data.get("show_currency_text"),
            reply_markup=keyboards.START_CHOOSE_MARKUP,
            parse_mode=ParseMode.MARKDOWN,
        )

        context.user_data.clear()
        return ConversationHandler.END

    if user_text == actions.TO_BACK:
        commands.show_start(update, context)
        context.user_data.clear()
        return ConversationHandler.END


def check_amount(update, context):
    user_text = update.message.text.replace(",", ".").strip()

    if user_text == actions.TO_BACK:
        commands.show_start(update, context)
        return ConversationHandler.END

    if utils.validate_amount(user_text):
        context.user_data["amount"] = user_text
        commands.show_amount_success(update, context)

        return FROM_RATE_STATE
    else:
        commands.incorrect_amount(update, context)
        return AMOUNT_STATE


def check_from_rate(update, context):
    user_text = update.message.text

    if user_text == actions.TO_BACK:
        commands.show_start(update, context)
        context.user_data.clear()
        return ConversationHandler.END

    user_text = user_text.upper()
    valute = utils.validate_valute_code(user_text)

    if valute:
        context.user_data["from_rate"] = valute
        context.user_data["from_rate_code"] = user_text

        commands.show_rate_success(update, context)

        return TO_RATE_STATE
    else:
        commands.incorrect_valute_code(update, context)
        return FROM_RATE_STATE


def check_to_rate(update, context):
    user_text = update.message.text

    if user_text == actions.TO_BACK:
        commands.show_start(update, context)
        context.user_data.clear()
        return ConversationHandler.END

    user_text = user_text.upper()
    valute = utils.validate_valute_code(user_text)

    if valute == "RUB":
        if context.user_data.get("from_rate_code") == "RUB":
            commands.unlogic_valute_code(update, context)
            return TO_RATE_STATE

    if valute:
        context.user_data["to_rate"] = valute
        context.user_data["to_rate_code"] = user_text

        update.message.reply_text(
            f"💰 *Cумма*:  {context.user_data.get('amount')}\n\n"
            f"⬅️ *Из валюты*:  {context.user_data.get('from_rate_code')}\n\n"
            f"➡️ *В валюту*:  {context.user_data.get('to_rate_code')}",
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=keyboards.CONVERTATION_CHOOSE_MARKUP,
        )
        return SHOW_CONVERTATION
    else:
        commands.incorrect_valute_code(update, context)
        return TO_RATE_STATE


def show_convertation(update, context):
    user_text = update.message.text

    if user_text == actions.TO_CONVERT:
        result = utils.convert(update, context.user_data)

        update.message.reply_text(
            text=f"Результат: `{result}` | *[{context.user_data.get('to_rate_code')}]*",
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=keyboards.START_CHOOSE_MARKUP,
        )

        context.user_data.clear()
        return ConversationHandler.END

    if user_text == actions.TO_BACK:
        commands.show_start(update, context)
        context.user_data.clear()
        return ConversationHandler.END
