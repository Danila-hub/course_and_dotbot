SHOW_CURRENCY = "Курс валюты 💹"
SHOW_CONVERTATION = "Конвертация 🔁"
SHOW_ALL_CURRENCIES = "🌎 Посмотреть на валюты 🌎"

TO_CONVERT = "Провести конвертацию 🔁"
TO_SHOW_CURRENCY = "Показать актуальный курс"
TO_BACK = "Назад"
