from json import load

from telegram import Update
from telegram.ext import (
    Updater,
    CallbackQueryHandler,
    Filters,
    MessageHandler,
    CommandHandler,
    ConversationHandler,
)

import logging_config
import commands
import states
from texts import actions


logging_config.setup_logging()

with open('config.json') as config_file:
    config = load(config_file)
    
BOT_TOKEN = config['BOT_TOKEN']


def start(update, context):
    commands.show_start(update, context)
    context.user_data.clear()

    return ConversationHandler.END


def cancel(update, context):
    commands.show_start(update, context)
    context.user_data.clear()

    return ConversationHandler.END


def start_bot():
    updater = Updater(BOT_TOKEN)
    updater.start_polling(timeout=10, read_latency=10)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(
        ConversationHandler(
            entry_points=[
                MessageHandler(
                    Filters.text & Filters.regex(f"^{actions.SHOW_CURRENCY}$"),
                    states.start_show_currency_entry,
                )
            ],
            states={
                states.CODE_CURRENCY_STATE: [
                    MessageHandler(
                        Filters.text & ~Filters.command, states.check_currency_code
                    )
                ],
                states.SHOW_CURRENCY: [
                    MessageHandler(
                        Filters.text & ~Filters.command, states.show_currency
                    )
                ],
            },
            fallbacks=[
                CommandHandler("cancel", cancel),
            ],
        )
    )

    dispatcher.add_handler(
        ConversationHandler(
            entry_points=[
                MessageHandler(
                    Filters.text & Filters.regex(f"^{actions.SHOW_CONVERTATION}$"),
                    states.start_convert_entry,
                )
            ],
            states={
                states.AMOUNT_STATE: [
                    MessageHandler(Filters.text & ~Filters.command, states.check_amount)
                ],
                states.FROM_RATE_STATE: [
                    MessageHandler(
                        Filters.text & ~Filters.command, states.check_from_rate
                    )
                ],
                states.TO_RATE_STATE: [
                    MessageHandler(
                        Filters.text & ~Filters.command, states.check_to_rate
                    )
                ],
                states.SHOW_CONVERTATION: [
                    MessageHandler(
                        Filters.text & ~Filters.command, states.show_convertation
                    )
                ],
            },
            fallbacks=[
                CommandHandler("cancel", cancel),
            ],
        )
    )

    dispatcher.add_handler(
        CallbackQueryHandler(
            commands.show_all_currencies, pattern="^show_all_currencies$"
        )
    )

    dispatcher.add_handler(
        MessageHandler(
            Filters.text & Filters.regex(f"^{actions.SHOW_ALL_CURRENCIES}$"),
            commands.show_all_currencies,
        )
    )

    dispatcher.add_handler(MessageHandler(Filters.text, start))

    updater.idle()


if __name__ == "__main__":
    start_bot()
